package edu.missouristate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import edu.missouristate.domain.Address;
import edu.missouristate.repository.AddressbookRepository;
import edu.missouristate.service.AddressbookService;

@Controller
public class AddressbookController {

	@Autowired
	AddressbookService addressbookService;

	@Autowired
	AddressbookRepository repository;

	@GetMapping(value = "/")
	public String getIndex(Model model) {
		List<Address> addressList = addressbookService.getAddressList();
		model.addAttribute("addressList", addressList);
		return "index";
	}

	@GetMapping(value = "/addAddress")
	public String getAddAddress(Model x) {

		return "addAddress";
	}

	@PostMapping(value = "/addAddress")
	public String postAddAddress(Model model, Address address) {
		addressbookService.saveAddress(address);
		return "redirect:/";
	}

	@GetMapping(value = "/sw.js")
	public String getSw(Model model) {
		return "";
	}
}
